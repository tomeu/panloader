Build mesa with panfrost. Clone panwrap into:

        mesa/src/panfrost/pandecode/

So you have a path like:

        /home/alyssa/mesa/src/panfrost/pandecode/panloader/panwrap

(Yes, really.)

Inside panwrap, make a build/ directory, cd in, meson .. . && ninja. You'll get
a libpanwrap.so out in build/panwrap/libpanwrap.so, upload that to the board
you're trying to trace and LD_PRELOAD it in to get a trace (including
disassembly of both Midgard and Bifrost).

If your name is Ryan, uncomment "#define dvalin" in include/mali-ioctl.h and panwrap will be built to trace a Dvalin kernel instead.
